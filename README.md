# Clone

```
git clone https://gitlab.com/m-server/auto-snapshot-server
```


# Proxy

Create secret
```
pwgen 64 1 > forwarding.secret
cat forwarding.secret
```


# Server

Configure secret

`config/config/paper-global.yml`
```
    secret: 'content of proxy forwarding.secret'
```


# Development

```
git clone git@gitlab.com:m-server/auto-snapshot-server.git
cd auto-snapshot-server
git update-index --assume-unchanged proxy/config/forwarding.secret
git update-index --assume-unchanged proxy/resumer/config.py
git update-index --assume-unchanged server/config/config/paper-global.yml
```
