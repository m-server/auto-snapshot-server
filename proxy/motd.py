line1 = '------- Auto Snapshot Server -------'
line2 = 'Only pay while you play, glowies included'
color1 = (0, 255, 127)
color2 = (127, 0, 255)
codes = ['d', '9', 'b', 'a', 'e', 6, 'c']
codes += codes[::-1]

def rgb_to_hex(rgb):
    return '#%02x%02x%02x' % rgb

line = ''

j = 0
lines = (line1, line2)
for li in range(len(lines)):
    l = lines[li]
    for i in range(len(l)):
        char = l[i]
        if char == '-':
            line += '&' + str(codes[j]) + char
            j += 1
        elif char != ' ':
            p = i / len(l)
            rgb = []
            for pos in range(3):
                diff = color2[pos] - color1[pos]
                value = int(color1[pos] + diff * p)
                rgb.append(value)

            color = rgb_to_hex(tuple(rgb))
            line += '&' + color + char
        else:
            line += char

    if li < (len(lines) - 1):
        line += '\n'

print(str(line.encode())[2:-1])
