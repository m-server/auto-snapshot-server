from hcloud import Client
from hcloud.server_types.domain import ServerType
from hcloud.servers.domain import ServerCreatePublicNetwork

import config


class Hetzner:
    def __init__(self):
        self.client = Client(token=config.token)
        self.locked = None

    def getPrimaryIp(self):
        primaryips = self.client.primary_ips.get_all()
        for ip in primaryips:
            if ip.ip == config.serverIp:
                return ip

        raise Exception('primary IP %s not found' % config.serverIp)

    def start(self):
        primaryip = self.getPrimaryIp()
        serverId = primaryip.assignee_id
        if serverId is not None:
            print('Server already running')
            return

        if self.locked:
            print('Another start/stop operation is already running')
            return

        try:
            self.locked = True
            images = self.client.images.get_all(
                    label_selector={'ip': primaryip.ip})

            if len(images) == 0:
                return

            image = images[0]
            servertype = ServerType(name=config.serverType)
            publicnetwork = ServerCreatePublicNetwork(
                    enable_ipv6 = False,
                    ipv4 = primaryip
            )
            response = self.client.servers.create(
                    primaryip.ip,
                    server_type = servertype,
                    public_net = publicnetwork,
                    datacenter = primaryip.datacenter,
                    image = image
            )

            print('Waiting for server to be created')
            response.action.wait_until_finished(3600)
            print('Deleting snapshot')
            image.delete()
            self.locked = False
        except Exception as e:
            self.locked = False
            raise e

    def stop(self):
        primaryip = self.getPrimaryIp()
        serverId = primaryip.assignee_id
        if serverId is None:
            print('Server already stopped')
            return

        if self.locked:
            print('Another start/stop operation is already running')
            return

        try:
            self.locked = True
            server = self.client.servers.get_by_id(serverId)
            shutdown = server.shutdown()
            shutdown.wait_until_finished(3600)
            response = server.create_image(labels={'ip': primaryip.ip})
            print('Waiting for snapshot to be created')
            response.action.wait_until_finished(3600)
            print('Deleting server')
            server.delete()
            self.locked = False
        except Exception as e:
            self.locked = False
            raise e
