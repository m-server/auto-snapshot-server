from mcstatus import JavaServer
from threading import Timer
from datetime import datetime
from time import sleep

from provider import Hetzner

import config


class Watchdog:
    def __init__(self, server, interval=1):
        self.endpoint = JavaServer.lookup(config.endpoint)
        self.server = server
        self.interval = interval
        self.delay = config.delay
        self.empty = None
        self.stopTimer = None
        self.online = None
        self.main()
    
    def stopServer(self):
        self.server.stop()
        self.online = False

    def startServer(self):
        if not self.online:
            self.server.start()
            self.online = True

    def main(self):
        Timer(self.interval, self.main).start()
        empty = self.isEmpty()
        if empty != self.empty:
            self.empty = empty
            if empty:
                self.stopTimer = Timer(self.delay, self.stopServer)
                self.stopTimer.start()
            else:
                if self.stopTimer:
                    self.stopTimer.cancel()
                    self.stopTimer = None

                self.startServer()

    def isEmpty(self):
        status = self.endpoint.status()
        if status.players.online == 0:
            return True

        return False


if __name__ == '__main__':
    server = Hetzner()
    watchdog = Watchdog(server)
